<?php
require_once "html-exception-handler.php";
require "auth-admin.php";
require_once "common.php";

if (!array_key_exists("id", $_GET)) exit;

$stmt = $dbh->prepare("SELECT * FROM news WHERE NewsID=:id");
$stmt->bindParam(":id", $_GET["id"]);
$stmt->execute();
$row = $stmt->fetch();

$title = $row['NewsTitle'];
$javascripts = array(
  "//tinymce.cachefly.net/4.0/tinymce.min.js",
  "//code.jquery.com/jquery-2.0.3.min.js",
  "js/jquery.blockUI.js",
  "js/edit-news.js"
);
include "header.php";
?>
<div class="two-column">
  <div class="main grid">
    <i class="grid-top-left"></i>
    <i class="grid-top-right"></i>
    <i class="grid-bottom-left"></i>
    <i class="grid-bottom-right"></i>
    <h1 id="newsTitle" class="editable"><?php echo $row['NewsTitle']; ?></h1>
    <h2>Date: <span id="newsDate" class="editable"><?php echo $row['NewsDate']; ?></span></h2>
    <div id="newsContent" class="editable"><?php echo $row['Content']; ?></div>
    <button onclick="saveAnnouncement('<?php echo $_GET["id"]; ?>')">Submit</button>
  </div>
  <div class="sidebar sprite paperclip1">
    <ul>
      <li class="active"><a href="announcements.php">Announcements</a></li>
      <li><a href="alumni.php">Distinguished Alumni</a></li>
      <li><a href="students.php">Current PhD Students</a></li>
    </ul>
  </div>
</div>
<?php include "footer.php"; ?>