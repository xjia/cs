<?php
require_once "common.php";

if (!array_key_exists("id", $_GET)) exit;

$stmt = $dbh->prepare("SELECT DirectionName,Publications FROM direction WHERE DirectionID=:id");
$stmt->bindParam("id", $_GET["id"]);
$stmt->execute();
$direction = $stmt->fetch();

$stmt = $dbh->prepare("SELECT DirectionID,DirectionName FROM direction ORDER BY Sequence");
$stmt->execute();
$directions = $stmt->fetchAll();

$title = $direction['DirectionName'];
include "header.php";
?>
<div class="two-column">
  <div class="main grid">
    <i class="grid-top-left"></i>
    <i class="grid-top-right"></i>
    <i class="grid-bottom-left"></i>
    <i class="grid-bottom-right"></i>
    <ul class="direction-links">
      <li class="first"><a href="direction.php?id=<?php echo $_GET['id']; ?>">Brief Intro</a></li>
      <li class="active">Selected Publications</li>
      <li><a href="labs.php?id=<?php echo $_GET['id']; ?>">Laboratories</a></li>
    </ul>
    <div class="direction-content"><?php echo $direction['Publications']; ?></div>
  </div>
  <div class="sidebar sprite paperclip4">
    <ul>
      <li class="active"><a href="pubs.php?id=<?php echo $_GET['id']; ?>"><?php echo $direction['DirectionName']; ?></a></li>
      <?php foreach ($directions as $d): ?>
        <?php if ($d['DirectionID'] != $_GET['id']): ?>
          <li><a href="pubs.php?id=<?php echo $d['DirectionID']; ?>"><?php echo $d['DirectionName']; ?></a></li>
        <?php endif; ?>
      <?php endforeach; ?>
    </ul>
  </div>
</div>
<?php include "footer.php"; ?>