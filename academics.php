<?php $title = "Academics"; include "header.php"; ?>
<div class="two-column">
  <div class="main grid academics">
    <i class="grid-top-left"></i>
    <i class="grid-top-right"></i>
    <i class="grid-bottom-left"></i>
    <i class="grid-bottom-right"></i>
    <h1>Academics</h1>
    <h2>BS in Computer Science and Technology</h2>
    <ul>
      <li>
        <a href="curriculum-bachelor.php">Curriculum</a>
        (<a href="data/Computer Science Curriculum.doc">word</a>,
         <a href="data/Computer Science Curriculum.pdf">pdf</a>)
      </li>
      <li>
        <a href="http://acm.sjtu.edu.cn/wiki/Courses">Program for ACM Class</a>
      </li>
    </ul>
    <h2>MS in Computer Science and Technology</h2>
    <ul>
      <li>
        <a href="curriculum-master.php">Curriculum</a>
      </li>
    </ul>
    <h2>PhD in Computer Science and Technology</h2>
    <ul>
      <li>
        <a href="curriculum-phd.php">Curriculum</a>
      </li>
      <li>
        <a href="news.php?id=79">计算机系09级及以后博士生要求（2项）</a>
      </li>
    </ul>
  </div>
  <div class="sidebar sprite paperclip2">
    <ul>
      <li class="active"><a href="academics.php">Academics</a></li>
      <li><a href="curriculum-bachelor.php">Undergraduate</a></li>
      <li><a href="curriculum-master.php">Master's Degree</a></li>
      <li><a href="curriculum-phd.php">Ph.D Degree</a></li>
    </ul>
  </div>
</div>
<?php include "footer.php"; ?>