<?php
require_once "config.php";
$dbh = new PDO('mysql:host='.DB_HOST.';dbname='.DB_NAME.';charset=utf8', DB_USER, DB_PASS);
$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$dbh->exec("set names utf8");

function active($variable, $target) {
  if ($variable == $target) {
    return ' class="active"';
  }
  return "";
}

function shorten_title($title, $length) {
  if (strlen($title) <= $length) {
    return $title;
  }
  return substr($title, 0, $length - 4) . " ...";
}

function redirect($location) {
  header("Location: $location", TRUE);
  exit;
}

function is_admin() {
  try {
    require "auth-admin.php";
    return TRUE;
  }
  catch (Exception $e) {
    return FALSE;
  }
}