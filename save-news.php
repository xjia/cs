<?php
require_once "json-exception-handler.php";
require "auth-admin.php";
require_once "common.php";

$stmt = $dbh->prepare("UPDATE news SET NewsTitle=:title, NewsDate=:date, Content=:content WHERE NewsID=:id");
$stmt->bindParam(":title", $_POST["title"]);
$stmt->bindParam(":date", $_POST["date"]);
$stmt->bindParam(":content", $_POST["content"]);
$stmt->bindParam(":id", $_POST["id"]);
$stmt->execute();

echo json_encode(array("status" => "ok"));
