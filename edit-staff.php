<?php
if (!array_key_exists("id", $_GET)) exit;

require_once "session-start.php";

if (!($_GET["id"] == $_SESSION["EntityID"] and $_SESSION["EntityType"] == "staff")) exit;

require_once "common.php";

$stmt = $dbh->prepare("SELECT * FROM staff WHERE StaffID=:id");
$stmt->bindParam(":id", $_GET["id"]);
$stmt->execute();
$person = $stmt->fetch();

$title = $person["Name"];
$people_type = $person["Type"];
$people_page = "people.php?type=$people_type";

$javascripts = array(
  "//tinymce.cachefly.net/4.0/tinymce.min.js",
  "//code.jquery.com/jquery-2.0.3.min.js",
  "js/jquery.blockUI.js",
  "js/edit-staff.js"
);
include "header.php";
?>
<div class="fullpage">
  <div class="main grid">
    <i class="grid-top-left"></i>
    <i class="grid-top-right"></i>
    <i class="grid-bottom-left"></i>
    <i class="grid-bottom-right"></i>
    <div class="sidebar"><?php include "people-sidebar.php"; ?></div>
    <div class="content">
      <ul class="letters">
        <?php for ($c = 'A'; $c <= 'Z'; $c = chr(ord($c) + 1)): ?>
        <?php if ($c == $person["Name"][0]): ?>
          <li class="highlight"><a href="<?php echo $people_page; ?>#<?php echo $c; ?>"><?php echo $c; ?></a></li>
        <?php else: ?>
          <li class="active"><a href="<?php echo $people_page; ?>#<?php echo $c; ?>"><?php echo $c; ?></a></li>
        <?php endif; ?>
        <?php endfor; ?>
      </ul>
      <div class="detail">
        <div class="detail-left">
          <img src=".<?php echo $person["Photo"]; ?>">
          <h1><span id="FamilyName" class="editable"><?php echo $person["FamilyName"]; ?></span> <span id="FirstName" class="editable"><?php echo $person["FirstName"]; ?></span></h2>
          <h2><span id="Position" class="editable"><?php echo $person["Position"]; ?></span> <span id="Degree" class="editable"><?php echo $person["Degree"]; ?></span></h2>
          <ul>
            <li>Office Tel: <span id="OfficeTel" class="editable"><?php echo $person["OfficeTel"]; ?></span></li>
            <li>Office Site: <span id="OfficeSiteBuilding" class="editable"><?php echo $person["OfficeSiteBuilding"]; ?></span>-<span id="OfficeSiteRoom" class="editable"><?php echo $person["OfficeSiteRoom"]; ?></span></li>
            <li>
              Email:
              <?php
              echo '<span id="Email" class="editable">'.$person["Email"].'</span>';
              if (strlen($person["OtherEmail"]) > 0)
                echo ', <span id="OtherEmail" class="editable">'.$person["OtherEmail"].'</span>';
              ?>
            </li>
            <li><button onclick="save('<?php echo $_GET["id"]; ?>')">Submit</button></li>
          </ul>
        </div>
        <div class="detail-right">
        <?php if ($person['Type'] == 'General Office' or $person['Type'] == 'Computer Center'): ?>
          <h2>Main Duties</h2>
          <span id="Duty" class="editable"><?php echo trim($person["Duty"]); ?></span>
        <?php else: ?>
          <h2>Homepage</h2>
          <span id="OtherLink" class="editable"><?php echo trim($person["OtherLink"]); ?></span>

          <h2>Research Area</h2>
          <span id="ResearchArea" class="editable"><?php echo nl2br(trim($person["ResearchArea"])); ?></span>

          <h2>Research Interest</h2>
          <p id="ResearchInterest" class="editable"><?php echo nl2br(trim($person["ResearchInterest"])); ?></p>

          <h2>Education Background</h2>
          <p id="EducationBackground" class="editable"><?php echo nl2br(trim($person["EducationBackground"])); ?></p>

          <h2>Work Experience</h2>
          <p id="WorkExperience" class="editable"><?php echo nl2br(trim($person["WorkExperience"])); ?></p>

          <h2>Teaching Assignment</h2>
          <p id="TeachingAssignment" class="editable"><?php echo nl2br(trim($person["TeachingAssignment"])); ?></p>

          <h2>Publications</h2>
          <p id="Publications" class="editable"><?php echo nl2br(trim($person["Publications"])); ?></p>

          <h2>Awards</h2>
          <p id="Awards" class="editable"><?php echo nl2br(trim($person["Awards"])); ?></p>

          <h2>Professional Service</h2>
          <p id="ProfessionalService" class="editable"><?php echo nl2br(trim($person["ProfessionalService"])); ?></p>

          <h2>Grants</h2>
          <p id="Grants" class="editable"><?php echo nl2br(trim($person["Grants"])); ?></p>
        <?php endif; ?>
        </div>
        <div class="clear"></div>
      </div>
    </div>
    <div class="clear"></div>
  </div>
</div>
<?php include "footer.php"; ?>