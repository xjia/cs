<?php
require_once "json-exception-handler.php";
require_once "session-start.php";

if (!($_POST["id"] == $_SESSION["EntityID"] and $_SESSION["EntityType"] == "student")) exit;

require_once "common.php";

$stmt = $dbh->prepare("UPDATE student SET FamilyName=:FamilyName, FirstName=:FirstName, ClassNum=:ClassNum, ContactInfo=:ContactInfo, Qmd=:Qmd, Email=:Email, Photo=:Photo WHERE StudentId=:id");
$stmt->bindParam(':FamilyName', $_POST["FamilyName"]);
$stmt->bindParam(':FirstName', $_POST["FirstName"]);
$stmt->bindParam(':ClassNum', $_POST["ClassNum"]);
$stmt->bindParam(':Qmd', $_POST["Qmd"]);
$stmt->bindParam(':ContactInfo', $_POST["ContactInfo"]);
$stmt->bindParam(':Email', $_POST["Email"]);
$stmt->bindParam(':Photo', $_POST["Photo"]);
$stmt->bindParam(":id", $_POST["id"]);
$stmt->execute();

echo json_encode(array("status" => "ok"));
