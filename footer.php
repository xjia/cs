<div class="footer sprite">
  <div class="footer-inner">
    <a class="csemp" href="http://www.csmoe.sjtu.edu.cn/">CS Engineering Master Program</a>
    <span class="contact">Contact: <a href="mailto:webmaster@cs.sjtu.edu.cn">webmaster@cs.sjtu.edu.cn</a></span>
    <span class="copy">Copyright &copy; 2013 SJTU-CSE</span>
  </div>
</div>
<?php if (isset($javascripts)): ?>
<?php foreach ($javascripts as $javascript): ?>
<script src="<?php echo $javascript; ?>"></script>
<?php endforeach; ?>
<?php endif; ?>
</body>
</html>