<?php
require_once "common.php";

$stmt = $dbh->prepare("SELECT DirectionID,DirectionName FROM direction ORDER BY Sequence");
$stmt->execute();
$directions = $stmt->fetchAll();

$title = "Research";
include "header.php";
?>
<div class="two-column">
  <div class="main grid research">
    <i class="grid-top-left"></i>
    <i class="grid-top-right"></i>
    <i class="grid-bottom-left"></i>
    <i class="grid-bottom-right"></i>
    <?php foreach ($directions as $direction): ?>
      <h2>
        <a href="direction.php?id=<?php echo $direction['DirectionID']; ?>">
          <?php echo $direction['DirectionName']; ?>
        </a>
      </h2>
      <ul>
        <li><a href="direction.php?id=<?php echo $direction['DirectionID']; ?>">Brief Intro</a></li>
        <li><a href="pubs.php?id=<?php echo $direction['DirectionID']; ?>">Selected Publications</a></li>
        <li><a href="labs.php?id=<?php echo $direction['DirectionID']; ?>">Laboratories</a></li>
      </ul>
    <?php endforeach; ?>
  </div>
  <div class="sidebar sprite paperclip1">
    <ul>
      <li class="active"><a href="research.php">Research</a></li>
      <li><a href="people.php">People</a></li>
      <li><a href="academics.php">Academics</a></li>
    </ul>
  </div>
</div>
<?php include "footer.php"; ?>