<?php
require_once "html-exception-handler.php";
require "auth-admin.php";

$title = 'Create Announcement';
$javascripts = array(
  "//tinymce.cachefly.net/4.0/tinymce.min.js",
  "//code.jquery.com/jquery-2.0.3.min.js",
  "js/jquery.blockUI.js",
  "js/edit-news.js"
);
include "header.php";
?>
<div class="two-column">
  <div class="main grid">
    <i class="grid-top-left"></i>
    <i class="grid-top-right"></i>
    <i class="grid-bottom-left"></i>
    <i class="grid-bottom-right"></i>
    <h1 id="newsTitle" class="editable">在此输入标题</h1>
    <h2>Date: <span id="newsDate" class="editable"><?php echo date('Y-m-d'); ?></span></h2>
    <div id="newsContent" class="editable">在此输入文章内容</div>
    <button onclick="create('announcement')">Submit</button>
  </div>
  <div class="sidebar sprite paperclip1">
    <ul>
      <li class="active"><a href="announcements.php">Announcements</a></li>
      <li><a href="alumni.php">Distinguished Alumni</a></li>
      <li><a href="students.php">Current PhD Students</a></li>
    </ul>
  </div>
</div>
<?php include "footer.php"; ?>