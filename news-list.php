<?php
require_once "common.php";

$stmt = $dbh->prepare("SELECT NewsID,NewsTitle,NewsDate FROM news WHERE newstype='News' ORDER BY NewsDate DESC");
$stmt->execute();
$rows = $stmt->fetchAll();

$title = "News";
include "header.php";
?>
<div class="two-column">
  <div class="main grid">
    <i class="grid-top-left"></i>
    <i class="grid-top-right"></i>
    <i class="grid-bottom-left"></i>
    <i class="grid-bottom-right"></i>
    <h1>News</h1>
    <?php if (is_admin()): ?>
      <a href="create-news.php">[create]</a>
    <?php endif; ?>
    <table class="news">
      <?php foreach ($rows as $row): ?>
        <tr>
          <td class="date">(<?php echo $row['NewsDate']; ?>)</td>
          <td class="title"><a href="news.php?id=<?php echo $row['NewsID']; ?>"><?php echo $row['NewsTitle']; ?></a></td>
        </tr>
      <?php endforeach; ?>
    </table>
  </div>
  <div class="sidebar sprite paperclip1">
    <ul>
      <li class="active"><a href="news-list.php">News</a></li>
      <li><a href="events.php">Events</a></li>
    </ul>
  </div>
</div>
<?php include "footer.php"; ?>