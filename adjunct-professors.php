<?php $title = "Adjunct Professors"; include "header.php"; ?>
<div class="fullpage">
  <div class="main grid">
    <i class="grid-top-left"></i>
    <i class="grid-top-right"></i>
    <i class="grid-bottom-left"></i>
    <i class="grid-bottom-right"></i>
    <div class="sidebar">
      <ul>
        <li><a href="people.php?type=Faculty">Faculty</a></li>
        <li><a href="people.php?type=Research Staff">Research Staff</a></li>
        <li class="active"><a href="adjunct-professors.php">Adjunct Professors</a></li>
        <li><a href="visiting-scholars.php">Visiting Scholars</a></li>
        <li><a href="people.php?type=General Office">General Office</a></li>
        <li><a href="people.php?type=Computer Center">Computer Center</a></li>
      </ul>
    </div>
    <div class="content">
      <ul class="letters">
        <?php for ($c = 'A'; $c <= 'Z'; $c = chr(ord($c) + 1)): ?>
          <li><?php echo $c; ?></li>
        <?php endfor; ?>
      </ul>
      <ul class="professors">
        <li class="professor">
          <img src="photos/visitors/image002.jpg">
          <div class="bio">
            <h1 class="name">Bo Li</h1>
            <h2 class="title">Chang-Jiang Chair Professor at SJTU</h2>
          </div>
          <p class="clear">
            Professor<br>
            Department of Computer Science and Engineering<br>
            The Hong Kong University of Science and Technology<br>
            <a href="http://www.cse.ust.hk/~bli/">http://www.cse.ust.hk/~bli/</a><br>
            <br>
            <b>Research Interests:</b><br>
            large-scale content distribution in the Internet, Peer-to-Peer media streaming, cloud computing, and wireless sensor networks.
          </p>
        </li>
        <li class="professor">
          <img src="photos/visitors/image004.jpg">
          <div class="bio">
            <h1 class="name">Bo Zhang</h1>
            <h2 class="title">Adjunct Professor at SJTU</h2>
          </div>
          <p class="clear">
            Member of Chinese Academy of Sciences<br>
            http://sourcedb.cas.cn/sourcedb_ad_cas/zw2/ysxx/xxjskxb/200906/t20090624_1807697.html<br>
            <br>
            <b>Research Interests:</b><br>
            - Artificial intelligence, artificial neural networks, genetic algorithms, fractal, wavelet theory.<br>
            - The application of applying the aforementioned theories into pattern recognition, knowledge engineering, robotics and intelligent control.
          </p>
        </li>
        <li class="professor">
          <img src="photos/visitors/image006.jpg">
          <div class="bio">
            <h1 class="name">Edwin (Hsing Mean) Sha</h1>
            <h2 class="title">Overseas Distinguished Young Scholar at SJTU</h2>
          </div>
          <p class="clear">
            Professor<br>
            Department of Computer Science<br>
            University of Texas at Dallas<br>
            <a href="http://www.utdallas.edu/~edsha/">http://www.utdallas.edu/~edsha/</a><br>
            <br>
            <b>Research Interests:</b><br>
            Embedded Software and Systems, Computer and Network Security, Parallel Architectures and Systems, High-performance and Low-Power Real-Time Systems, Network Architectures, Compilers, Application Specific VLSI Design, Operating Systems, High-Level Synthesis.
          </p>
        </li>
        <li class="professor">
          <img src="photos/visitors/image010.jpg">
          <div class="bio">
            <h1 class="name">Jie Wu</h1>
            <h2 class="title">Overseas Distinguished Young Scholar at SJTU</h2>
          </div>
          <p class="clear">
            Chairman and Professor<br>
            Department of Computer and Information Science<br>
            Temple University<br>
            <a href="http://www.cis.temple.edu/~wu/">http://www.cis.temple.edu/~wu/</a><br>
            <br>
            <b>Research Interests:</b><br>
            - Mobile Computing and Wireless Networks<br>
            - Routing Protocols<br>
            - Computer and Network Security<br>
            - Distributed Computing<br>
            - Fault-Tolerant Systems<br>
            - Parallel Processing<br>
            - Interconnection Networks
          </p>
        </li>
        <li class="professor">
          <img src="photos/visitors/image014.jpg">
          <div class="bio">
            <h1 class="name">Lionel M. NI</h1>
            <h2 class="title">Distinguished Professor at SJTU<br>Thousand-Person Plan</h2>
          </div>
          <p class="clear">
            Chair Professor<br>
            Department of Computer Science and Engineering<br>
            Hong Kong University of Science and Technology<br>
            <a href="http://www.cse.ust.hk/~ni/">http://www.cse.ust.hk/~ni/</a><br>
            <br>
            <b>Research Interests:</b><br>
            Wireless sensor networks; Pervasive/Ubiquitous computing; Grid computing; Peer-to-peer computing; Mobile computing; High-speed networking; High-performance computer architecture; Parallel and distributed systems; Multicore computing; Network security.
          </p>
        </li>
      </ul>
    </div>
    <div class="clear"></div>
  </div>
</div>
<?php include "footer.php"; ?>