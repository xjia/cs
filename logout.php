<?php
require_once "session-start.php";
session_unset();
header('Location: ' . $_SERVER['HTTP_REFERER']);
