<?php
require_once "common.php";

$stmt = $dbh->prepare("SELECT NewsID,NewsTitle,NewsDate FROM news WHERE newstype='Announcements' ORDER BY NewsDate DESC");
$stmt->execute();
$rows = $stmt->fetchAll();

$title = "Announcements";
include "header.php";
?>
<div class="two-column">
  <div class="main grid">
    <i class="grid-top-left"></i>
    <i class="grid-top-right"></i>
    <i class="grid-bottom-left"></i>
    <i class="grid-bottom-right"></i>
    <h1>Announcements</h1>
    <?php if (is_admin()): ?>
      <a href="create-announcement.php">[create]</a>
    <?php endif; ?>
    <table class="news">
      <?php foreach ($rows as $row): ?>
        <tr>
          <td class="date">(<?php echo $row['NewsDate']; ?>)</td>
          <td class="title"><a href="announcement.php?id=<?php echo $row['NewsID']; ?>"><?php echo $row['NewsTitle']; ?></a></td>
        </tr>
      <?php endforeach; ?>
    </table>
  </div>
  <div class="sidebar sprite paperclip1">
    <ul>
      <li class="active"><a href="announcements.php">Announcements</a></li>
      <li><a href="alumni.php">Distinguished Alumni</a></li>
      <li><a href="students.php">Current PhD Students</a></li>
    </ul>
  </div>
</div>
<?php include "footer.php"; ?>