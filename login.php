<?php
require_once "html-exception-handler.php";
require_once "common.php";

function authenticate($username, $password) {
  $ldaprdn  = "uid=$username,ou=people,dc=cs,dc=sjtu,dc=edu,dc=cn";
  $ldaphost = "202.120.38.143";
  $ldapport = 389;
  $ds = ldap_connect($ldaphost, $ldapport);
  if ($ds) {
    ldap_set_option($ds, LDAP_OPT_PROTOCOL_VERSION, 3);
    return @ldap_bind($ds, $ldaprdn, $password);
  }
  return FALSE;
}

require_once "ldap-mapping.php";

if (!array_key_exists($_POST["username"], $LDAP_MAPPING)) {
  throw new Exception("User not found.");
}
if (!authenticate($_POST["username"], $_POST["password"])) {
  throw new Exception("Login failed.");
}
$userid = $LDAP_MAPPING[$_POST["username"]];

require_once "session-start.php";
$_SESSION['Username'] = $_POST["username"];
$_SESSION['EntityID'] = $userid;
$_SESSION['EntityType'] = 'staff';

try {
  require "auth-admin.php";
  redirect("dashboard.php");
}
catch (Exception $e) {
  redirect("edit-staff.php?id=$userid");
}
