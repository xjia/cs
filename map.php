<?php $title = "Visit CS"; include "header.php"; ?>
<div class="two-column">
  <div class="main grid">
    <i class="grid-top-left"></i>
    <i class="grid-top-right"></i>
    <i class="grid-bottom-left"></i>
    <i class="grid-bottom-right"></i>
    <h1>Travel to SJTU</h1>
    <p>
      <a href="i/maps/how_to_find_sjtu.jpg">
        <img src="i/maps/how_to_find_sjtu_thumbnail.jpg">
      </a>
    </p>
    <h1>Travel to CSE</h1>
    <p>
      <a href="i/maps/how_to_find_cs.jpg">
        <img src="i/maps/how_to_find_cs_thumbnail.jpg">
      </a>
    </p>
    <h1>Contact Us</h1>
    <p>
      Department of Computer Science &amp; Engineering<br>
      Shanghai Jiao Tong University<br>
      SEIEE Building #04-417<br>
      800 Dongchuan Road<br>
      Shanghai 200240<br>
      P.R. China<br>
    </p>
    <p>
      Phone: +86-21-3420-6678<br>
      Fax: +86-21-34204728<br>
      Office Hours:<br>
      Monday-Friday 8:30am-11:30am, 1:00pm-5:00pm<br>
    </p>
  </div>
  <div class="sidebar sprite paperclip1">
    <ul>
      <li class="active"><a href="map.php">Visit CS</a></li>
      <li><a href="job.php">Job Opportunities</a></li>
      <li><a href="yellow-page.php">Yellow Pages</a></li>
    </ul>
  </div>
</div>
<?php include "footer.php"; ?>