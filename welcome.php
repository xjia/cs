<?php $title = "Welcome"; include "header.php"; ?>
<div class="two-column">
  <div class="main grid">
    <i class="grid-top-left"></i>
    <i class="grid-top-right"></i>
    <i class="grid-bottom-left"></i>
    <i class="grid-bottom-right"></i>
    <h1>Welcome from the Head of Department</h1>
    <p>Welcome to Shanghai Jiao Tong University’s Department of Computer Science and Engineering (CSE)! CSE Department is one of the premier institutions in China for computer science research and education. Founded in 1984, CSE Department offers the degrees Bachelor, Master and Doctor of Computer Science. The department is a center for research and education at the undergraduate and graduate levels. Strong research groups exist in areas of theoretical computer science, cryptography, parallel and distributed systems, networking and artificial intelligence, etc. Following the general criteria in the world, CSE department focuses on pursuing a top-ranking research institution in some areas of computer science.</p>
    <p>Recently, we are organizing a whole-english-based curriculum for the undergraduate and graduate degree programs, to attract international students and to prepare students to be industry and academic leaders who can apply technology and computer science principles across a wide variety of fields. We also provide a series of new courses as the rapid change of IT knowledge and the need from industry.</p>
    <p>We are trying our best to contribute to our nation’s economic development of IT industry and academic advancement in computer science. We hope to enter the top tier in research and Ph.D education in the world in the next decade.</p>
    <img class="signature-en" src="i/signature_en.png">
    <img class="signature-cn" src="i/signature_cn.png">
  </div>
  <div class="sidebar sprite paperclip2">
    <ul>
      <li class="active"><a href="welcome.php">Welcome Message</a></li>
      <li><a href="mission.php">Mission</a></li>
      <li><a href="history.php">History</a></li>
      <li><a href="organization.php">Organization</a></li>
      <li><a href="international.php">Global CS</a></li>
    </ul>
  </div>
</div>
<?php include "footer.php"; ?>