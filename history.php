<?php $title = "History"; include "header.php"; ?>
<div class="two-column">
  <div class="main grid">
    <i class="grid-top-left"></i>
    <i class="grid-top-right"></i>
    <i class="grid-bottom-left"></i>
    <i class="grid-bottom-right"></i>
    <h1>About Department of Computer Science and Engineering, SJTU</h1>
    <p>In 1956, under the auspice of China’s nuclear initiatives, Shanghai Jiao Tong University (SJTU) became one of the first three universities in China to offer a computer science major, beginning with merely five faculty members. The computer science at SJTU has since steadily developed along with China’s defense and technology industries. In 1984, SJTU’s Department of Computer Science and Engineering (SJTU-CSE) was founded as part of the School of Electrical and Electronic Engineering. From that moment, SJTU-CSE has started its integration into the world, through extensive academic exchanges and research collaborations. The joint project with Technical University of Berlin which resulted in China’s first “high-performance micro-super computer” in 1992 and the new curriculum based on ACM/IEEE standards since 1994 are just some of the examples during this period.</p>
    <p>With the turn of the century, SJTU-CSE has sped up its process of internationalization. Helped by the quality new faculty members recruited from leading institutes around the world, and based on the projected demands and growth of the IT industry, the department has established five core research areas: computer science theory, parallel and distributed computing, intelligence and human computer interaction, cryptography and information security and computer applications. Our faculty members has published over 700 high quality research papers in venues such as IEEE Transactions on Information Theory, IEEE Transactions on Neural Networks, IEEE Transactions on Signal Processing, IEEE Transactions on Fussy Systems, Neural Networks, Theoretical Computer Science, Information and Computation, Pattern Recognition, IEEE Transactions on Medical Images, IEEE Transactions on Multimedia, Computer Networks, IEEE InfoCom and WWW. Our research groups have also conducted research under the sponsorships of major national research funds including NSFC, 863 and 973. The department adopts course structures and curriculums that are similar to the top computer science departments in the world. Our students not only laid strong theoretical foundations here but also develop skills to learn, do research and innovate independently. For example, SJTU-CSE undergraduates came out top in the ACM International Collegiate Programming Context in 2002 and 2005. Our undergraduate and graduate programs are tailored to each individual student’s interests and characteristics. As a result, our graduates have every opportunity to emerge as promising researchers or technical leaders.</p>
    <p>In recent years, the department has started joint degree/diploma programs with Université de Paris, Technical University of Berlin, University of Tokyo, National University of Singapore and Ohio State University. It also has long-term and on-going collaborations with leading universities and research institutes in Japan, France and German in a variety of areas such as programming theories, artificial brain theories and modeling, machine learning and computational linguistics, semantics and system biology formalism and complexity theory. Moreover, the department cooperated with global high-tech companies such as Microsoft and Hitachi on both education and research. Most notably, the SJTU-Microsoft Joint Lab for Intelligent Computing and Intelligent Systems was granted Ministry of Education-Microsoft Key Research Lab status for its extraordinary mode of collaboration and research outputs.</p>
    <p>In 2007, SJTU-CSE was ranked 6th place in China by the Ministry of Education. In 2009, for the first time, Department of Computer Science and Engineering of Shanghai Jiao Tong University made it into the top 100 of the Academic Rankings of World Universities (ARWU).</p>
  </div>
  <div class="sidebar sprite paperclip2">
    <ul>
      <li class="active"><a href="history.php">History</a></li>
      <li><a href="welcome.php">Welcome Message</a></li>
      <li><a href="mission.php">Mission</a></li>
      <li><a href="organization.php">Organization</a></li>
      <li><a href="international.php">Global CS</a></li>
    </ul>
  </div>
</div>
<?php include "footer.php"; ?>