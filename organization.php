<?php $title = "Organization"; include "header.php"; ?>
<div class="two-column">
  <div class="main grid">
    <i class="grid-top-left"></i>
    <i class="grid-top-right"></i>
    <i class="grid-bottom-left"></i>
    <i class="grid-bottom-right"></i>
    <h1>Organization Chart</h1>
    <p>
      <a href="i/organization_chart.jpg">
        <img src="i/organization_chart_thumbnail">
      </a>
    </p>
  </div>
  <div class="sidebar sprite paperclip2">
    <ul>
      <li class="active"><a href="organization.php">Organization</a></li>
      <li><a href="welcome.php">Welcome Message</a></li>
      <li><a href="mission.php">Mission</a></li>
      <li><a href="history.php">History</a></li>
      <li><a href="international.php">Global CS</a></li>
    </ul>
  </div>
</div>
<?php include "footer.php"; ?>