<?php
require_once "json-exception-handler.php";
require_once "session-start.php";

if (!($_POST["id"] == $_SESSION["EntityID"] and $_SESSION["EntityType"] == "staff")) exit;

require_once "common.php";

$stmt = $dbh->prepare("UPDATE staff SET Name=:Name, FamilyName=:FamilyName, FirstName=:FirstName, Position=:Position, Degree=:Degree, OfficeTel=:OfficeTel, OfficeSiteBuilding=:OfficeSiteBuilding, OfficeSiteRoom=:OfficeSiteRoom, OtherLink=:OtherLink, Email=:Email, OtherEmail=:OtherEmail, ResearchArea=:ResearchArea, ResearchInterest=:ResearchInterest, EducationBackground=:EducationBackground, WorkExperience=:WorkExperience, TeachingAssignment=:TeachingAssignment, Publications=:Publications, Awards=:Awards, ProfessionalService=:ProfessionalService, Grants=:Grants, Duty=:Duty WHERE StaffID=:id");
$name = $_POST["FamilyName"].", ".$_POST["FirstName"];
$stmt->bindParam(":Name", $name);
$stmt->bindParam(':FamilyName', $_POST["FamilyName"]);
$stmt->bindParam(':FirstName', $_POST["FirstName"]);
$stmt->bindParam(':Position', $_POST["Position"]);
$stmt->bindParam(':Degree', $_POST["Degree"]);
$stmt->bindParam(':OfficeTel', $_POST["OfficeTel"]);
$stmt->bindParam(':OfficeSiteBuilding', $_POST["OfficeSiteBuilding"]);
$stmt->bindParam(':OfficeSiteRoom', $_POST["OfficeSiteRoom"]);
$stmt->bindParam(':Email', $_POST["Email"]);
$stmt->bindParam(':OtherEmail', $_POST["OtherEmail"]);
$stmt->bindParam(':OtherLink', $_POST["OtherLink"]);
$stmt->bindParam(':ResearchArea', $_POST["ResearchInterest"]);
$stmt->bindParam(':ResearchInterest', $_POST["ResearchInterest"]);
$stmt->bindParam(':EducationBackground', $_POST["EducationBackground"]);
$stmt->bindParam(':WorkExperience', $_POST["WorkExperience"]);
$stmt->bindParam(':TeachingAssignment', $_POST["TeachingAssignment"]);
$stmt->bindParam(':Publications', $_POST["Publications"]);
$stmt->bindParam(':Awards', $_POST["Awards"]);
$stmt->bindParam(':ProfessionalService', $_POST["ProfessionalService"]);
$stmt->bindParam(':Grants', $_POST["Grants"]);
$stmt->bindParam(':Duty', $_POST["Duty"]);
$stmt->bindParam(":id", $_POST["id"]);
$stmt->execute();

echo json_encode(array("status" => "ok"));
