<?php $title = "File Download"; include "header.php"; ?>
<div class="two-column">
  <div class="main grid">
    <i class="grid-top-left"></i>
    <i class="grid-top-right"></i>
    <i class="grid-bottom-left"></i>
    <i class="grid-bottom-right"></i>
    <h1>File Download</h1>
    <ol class="files">
      <li><a href="files/1.doc">Paper Modification Reg Form</a></li>
      <li><a href="files/2.html">电子信息与电气工程学院本科教学工作条例</a></li>
      <li><a href="files/非考试周考试安排登记表.xls">非考试周考试安排登记表</a></li>
      <li><a href="files/4.xls">课程调整申请表模版</a></li>
      <li><a href="files/5.doc">课程考试命题审批表</a></li>
      <li><a href="files/6.doc">试卷模板－新(A4)2007-1-8</a></li>
      <li><a href="files/7.doc">硕士和博士论文题目修改表</a></li>
      <li><a href="files/8.doc">研究生助教岗位考评细则表</a></li>
      <li><a href="files/9.doc">研究生助教岗位职责协议书</a></li>
    </ol>
  </div>
  <div class="sidebar sprite paperclip2">
    <ul>
      <li class="active"><a href="filedownload.php">File Download</a></li>
      <li><a href="map.php">Visit CS</a></li>
      <li><a href="job.php">Job Opportunities</a></li>
      <li><a href="yellow-page.php">Yellow Pages</a></li>
    </ul>
  </div>
</div>
<?php include "footer.php"; ?>