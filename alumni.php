<?php
$title = "Alumni";
include "header.php";
?>
<div class="two-column">
  <div class="main grid">
    <i class="grid-top-left"></i>
    <i class="grid-top-right"></i>
    <i class="grid-bottom-left"></i>
    <i class="grid-bottom-right"></i>
    <table class="alumni">
      <tr>
        <td class="photo">
          <img src="photos/alumni/image001.jpg">
        </td>
        <td class="info">
          <h2>MAO, Daolin 茅道临</h2>
          <p>
            Former CEO of Sina<br>
            Graduated with BS in Computer Science, SJTU in 1985.<br>
            <a href="http://baike.baidu.com/view/47409.htm">http://baike.baidu.com/view/47409.htm</a>
          </p>
        </td>
      </tr>
      <tr>
        <td class="photo">
          <img src="photos/alumni/image002.jpg">
        </td>
        <td class="info">
          <h2>YANG, Yuanqing 杨元庆</h2>
          <p>
            CEO, Lenovo<br>
            Graduated with BS in Computer Science, SJTU in 1986.<br>
            <a href="http://baike.baidu.com/view/40355.htm">http://baike.baidu.com/view/40355.htm</a>
          </p>
        </td>
      </tr>
      <tr>
        <td class="photo">
          <img src="photos/alumni/image003.jpg">
        </td>
        <td class="info">
          <h2>LU, Yimin 陆益民</h2>
          <p>
            Vice Chairman of the Board and CEO, China Unicom<br>
            Graduated with BS in Computer Science, SJTU in 1985.<br>
            <a href="http://tech.163.com/mobile/10/0511/13/66DIH75O00112K8E.html">http://tech.163.com/mobile/10/0511/13/66DIH75O00112K8E.html</a>
          </p>
        </td>
      </tr>
      <tr>
        <td class="photo">
          <img src="photos/alumni/image004.jpg">
        </td>
        <td class="info">
          <h2>TENG, Shang-Hua 滕尚华</h2>
          <p>
            2008 <a href="http://en.wikipedia.org/wiki/G%C3%B6del_Prize">Gödel Prize</a> Laureate<br>
            Graduated with BS in Computer Science, SJTU in 1985.<br>
            <a href="http://www.cs.bu.edu/~steng/">http://www.cs.bu.edu/~steng/</a>
          </p>
        </td>
      </tr>
      <tr>
        <td class="photo">
          <img src="photos/alumni/image005.jpg">
        </td>
        <td class="info">
          <h2>ZHAO, Jianjun 赵建军</h2>
          <p>
            Founder and Chairman of TP-Link Technologies Co. Ltd.<br>
            Graduated with MS in Computer Science, SJTU in 1985.<br>
            <a href="http://www.sjtufa.org/news_events/0909alumni_summit_speakers_2.htm">http://www.sjtufa.org/news_events/0909alumni_summit_speakers_2.htm</a>
          </p>
        </td>
      </tr>
    </table>
  </div>
  <div class="sidebar sprite paperclip1">
    <ul>
      <li class="active"><a href="alumni.php">Distinguished Alumni</a></li>
      <li><a href="announcements.php">Announcements</a></li>
      <li><a href="students.php">Current PhD Students</a></li>
    </ul>
  </div>
</div>
<?php include "footer.php"; ?>