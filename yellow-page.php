<?php
require_once "common.php";

if (array_key_exists("type", $_GET)) {
  $people_type = $_GET["type"];
}
else {
  $people_type = "Faculty";
}

$stmt = $dbh->prepare("SELECT * FROM staff WHERE Type=:type ORDER BY Name");
$stmt->bindParam(":type", $people_type);
$stmt->execute();

$title = "$people_type :: Yellow Pages";
include "header.php";
?>
<div class="two-column">
  <div class="main grid">
    <i class="grid-top-left"></i>
    <i class="grid-top-right"></i>
    <i class="grid-bottom-left"></i>
    <i class="grid-bottom-right"></i>
    <h1>Yellow Page of <?php echo $people_type; ?></h1>
    <table class="yellow-page">
      <tr>
        <th>Name</th>
        <th>Office Tel No</th>
        <th>Lab Tel No</th>
      </tr>
      <?php while ($row = $stmt->fetch()): ?>
      <tr>
        <td><?php echo $row['Name']; ?></td>
        <td><?php echo $row['OfficeTel']; ?></td>
        <td><?php echo $row['LabTelNo']; ?></td>
      </tr>
      <?php endwhile; ?>
    </table>
  </div>
  <div class="sidebar sprite paperclip2">
    <ul>
      <li class="active"><a>Yellow Pages</a></li>
      <li><a href="yellow-page.php?type=Faculty">Faculty</a></li>
      <li><a href="yellow-page.php?type=Research Staff">Research Staff</a></li>
      <li><a href="yellow-page.php?type=General Office">General Office</a></li>
      <li><a href="yellow-page.php?type=Computer Center">Computer Center</a></li>
    </ul>
  </div>
</div>
<?php include "footer.php"; ?>