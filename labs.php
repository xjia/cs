<?php
require_once "common.php";

if (!array_key_exists("id", $_GET)) exit;

$stmt = $dbh->prepare("SELECT * FROM lab WHERE DirectionID=:id ORDER BY LabName");
$stmt->bindParam("id", $_GET["id"]);
$stmt->execute();
$labs = $stmt->fetchAll();

$stmt = $dbh->prepare("SELECT DirectionID,DirectionName FROM direction ORDER BY Sequence");
$stmt->execute();
$directions = $stmt->fetchAll();

foreach ($directions as $direction) {
  if ($direction['DirectionID'] == $_GET['id']) {
    $title = $direction['DirectionName'];
  }
}
include "header.php";
?>
<div class="two-column">
  <div class="main grid">
    <i class="grid-top-left"></i>
    <i class="grid-top-right"></i>
    <i class="grid-bottom-left"></i>
    <i class="grid-bottom-right"></i>
    <ul class="direction-links">
      <li class="first"><a href="direction.php?id=<?php echo $_GET['id']; ?>">Brief Intro</a></li>
      <li><a href="pubs.php?id=<?php echo $_GET['id']; ?>">Selected Publications</a></li>
      <li class="active">Laboratories</li>
    </ul>
    <div class="direction-content">
      <ul class="labs">
        <?php foreach ($labs as $lab): ?>
          <?php
          $stmt = $dbh->prepare("SELECT staff.StaffID AS StaffID, staff.FamilyName AS FamilyName, staff.FirstName AS FirstName FROM lab_staff,staff WHERE lab_staff.LabID=:id AND lab_staff.StaffID=staff.StaffID ORDER BY staff.Name");
          $stmt->bindParam("id", $lab['LabID']);
          $stmt->execute();
          $people = $stmt->fetchAll();
          ?>
          <li id="lab<?php echo $lab['LabID']; ?>">
            <a class="name" href="<?php echo $lab['Link']; ?>"><?php echo $lab['LabName']; ?></a><br>
            People:
            <?php foreach ($people as $i => $person): ?><?php if ($i > 0) echo ","; ?>
              <a href="people-detail.php?id=<?php echo $person['StaffID']; ?>">
                <?php echo $person['FamilyName'] . ' ' . $person['FirstName']; ?></a><?php endforeach; ?>
            <br>
            Tel: <?php echo $lab['Tel']; ?><br>
            Site: <?php echo $lab['Room']; ?>, <?php echo $lab['Building']; ?>
          </li>
        <?php endforeach; ?>
      </ul>
    </div>
  </div>
  <div class="sidebar sprite paperclip4">
    <ul>
      <li class="active"><a href="labs.php?id=<?php echo $_GET['id']; ?>"><?php echo $title; ?></a></li>
      <?php foreach ($directions as $d): ?>
        <?php if ($d['DirectionID'] != $_GET['id']): ?>
          <li><a href="labs.php?id=<?php echo $d['DirectionID']; ?>"><?php echo $d['DirectionName']; ?></a></li>
        <?php endif; ?>
      <?php endforeach; ?>
    </ul>
  </div>
</div>
<?php include "footer.php"; ?>