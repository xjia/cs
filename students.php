<?php
require_once "common.php";

$stmt = $dbh->prepare("SELECT * FROM student WHERE Photo IS NOT NULL AND FamilyName<>'' AND FirstName<>'' AND StudentType='Ph.D.' ORDER BY FamilyName,FirstName");
$stmt->execute();
$rows = $stmt->fetchAll();

$title = "Students";
include "header.php";
?>
<div class="two-column">
  <div class="main grid">
    <i class="grid-top-left"></i>
    <i class="grid-top-right"></i>
    <i class="grid-bottom-left"></i>
    <i class="grid-bottom-right"></i>
    <table class="alumni">
      <?php foreach ($rows as $row): ?>
      <tr id="student<?php echo $row['StudentId']; ?>">
        <td class="photo">
        <?php if (strpos($row['Photo'], 'http') === 0): ?>
          <img src="<?php echo $row['Photo']; ?>">
        <?php else: ?>
          <img src=".<?php echo $row['Photo']; ?>">
        <?php endif; ?>
        </td>
        <td class="info">
          <h2><?php echo $row['FamilyName']; ?>, <?php echo $row['FirstName']; ?></h2>
          <p>
            <?php if (strlen($row['ClassNum']) > 0): ?>
              <b>Class Number:</b>
              <?php echo $row['ClassNum']; ?>
              <br>
            <?php endif; ?>
            <?php if (strlen(trim($row['Qmd'])) > 0): ?>
              <b>QMD:</b>
              <?php echo nl2br(trim($row['Qmd'])); ?>
              <br>
            <?php endif; ?>
            <?php if (strlen(trim($row['ContactInfo'])) > 0): ?>
              <b>Contact:</b>
              <?php echo nl2br(trim($row['ContactInfo'])); ?>
              <br>
            <?php endif; ?>
            <?php if (strlen(trim($row['Email'])) > 0): ?>
              <b>Email:</b>
              <?php echo str_replace('@', '[at]', $row['Email']); ?>
            <?php endif; ?>
          </p>
        </td>
      </tr>
      <?php endforeach; ?>
    </table>
  </div>
  <div class="sidebar sprite paperclip1">
    <ul>
      <li class="active"><a href="students.php">Current PhD Students</a></li>
      <li><a href="announcements.php">Announcements</a></li>
      <li><a href="alumni.php">Distinguished Alumni</a></li>
    </ul>
  </div>
</div>
<?php include "footer.php"; ?>