<?php
require_once "json-exception-handler.php";
require "auth-admin.php";
require_once "common.php";

$type = $_POST["type"];
if ($type == 'news') $type = 'News';
if ($type == 'event') $type = 'Events';
if ($type == 'announcement') $type = 'Announcements';

$stmt = $dbh->prepare("INSERT INTO news(newstype,NewsTitle,NewsDate,Content) VALUES(:type,:title,:date,:content)");
$stmt->bindParam(":type", $type);
$stmt->bindParam(":title", $_POST["title"]);
$stmt->bindParam(":date", $_POST["date"]);
$stmt->bindParam(":content", $_POST["content"]);
$stmt->execute();

echo json_encode(array("status" => "ok", "newsId" => $dbh->lastInsertId()));
