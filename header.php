<?php require_once "session-start.php"; ?><!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title><?php echo $title; ?> :: SJTU Computer Science &amp; Engineering</title>
  <!--[if lt IE 9]>
  <script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE9.js"></script>
  <![endif]-->
  <link rel="stylesheet" type="text/css" href="i/all.css">
</head>
<body>
<div class="header">
  <a href="index.php">
    <h1 class="sprite title-left">SJTU Department of Computer Science &amp; Engineering</h1>
    <h1 class="sprite title-right">上海交通大学 计算机科学与工程系</h1>
  </a>
</div>
<div class="menu sprite">
  <ul class="menu-left">
    <li><a href="welcome.php">About</a></li>
    <li><a href="people.php">People</a></li>
    <li><a href="announcements.php">Students</a></li>
  </ul>
  <ul class="menu-right">
    <li><a href="academics.php">Academics</a></li>
    <li><a href="research.php">Research</a></li>
    <li><a href="filedownload.php">Shortcuts</a></li>
  </ul>
  <div class="form-left">
    <?php if ($_SESSION && array_key_exists("Username", $_SESSION)): ?>
      <form action="logout.php" method="POST">
        <label>
          Current <?php echo $_SESSION['EntityType']; ?>:
          <a href="edit-<?php echo $_SESSION['EntityType']; ?>.php?id=<?php echo $_SESSION['EntityID']; ?>"><?php echo $_SESSION["Username"]; ?></a>
        </label>
        <input type="submit" class="sprite login-button" value="logout">
      </form>
    <?php else: ?>
      <form action="login.php" method="POST">
        <label>NAME</label>
        <input type="text" name="username" class="sprite login-input">
        <label>PASSWORD</label>
        <input type="password" name="password" class="sprite login-input">
        <input type="submit" class="sprite login-button" value="login">
      </form>
    <?php endif; ?>
  </div>
  <div class="form-right">
    <form action="http://www.google.com/cse" id="cse-search-box">
      <input type="hidden" name="cx" value="017917172514735012166:kz3-lpwlumc">
      <input type="hidden" name="ie" value="UTF-8">
      <input type="text" name="q" placeholder="Search" class="sprite search-input">
      <input type="submit" name="sa" class="sprite search-button">
    </form>
  </div>
</div>
