<?php
require_once "session-start.php";

if (!array_key_exists("Username", $_SESSION)) {
  throw new Exception("Authentication required.");
}

require_once "config.php";

if (strpos(ADMINS, '|'.$_SESSION["Username"].'|') === FALSE) {
  throw new Exception("Authorization required.");
}
