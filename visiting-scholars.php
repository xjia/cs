<?php $title = "Visiting Scholars"; include "header.php"; ?>
<div class="fullpage">
  <div class="main grid">
    <i class="grid-top-left"></i>
    <i class="grid-top-right"></i>
    <i class="grid-bottom-left"></i>
    <i class="grid-bottom-right"></i>
    <div class="sidebar">
      <ul>
        <li><a href="people.php?type=Faculty">Faculty</a></li>
        <li><a href="people.php?type=Research Staff">Research Staff</a></li>
        <li><a href="adjunct-professors.php">Adjunct Professors</a></li>
        <li class="active"><a href="visiting-scholars.php">Visiting Scholars</a></li>
        <li><a href="people.php?type=General Office">General Office</a></li>
        <li><a href="people.php?type=Computer Center">Computer Center</a></li>
      </ul>
    </div>
    <div class="content">
      <ul class="letters">
        <?php for ($c = 'A'; $c <= 'Z'; $c = chr(ord($c) + 1)): ?>
          <li><?php echo $c; ?></li>
        <?php endfor; ?>
      </ul>
      <ul class="professors">
        <li class="professor">
          <img src="photos/visitors/image001.jpg">
          <div class="bio">
            <h1 class="name">Wei Shu</h1>
            <h2 class="title">Associate Professor</h2>
          </div>
          <p class="clear">
            Associate Chair, Director of Graduate Program<br>
            Electrical &amp; Computer Engineering Dept.<br>
            University of New Mexico, US<br>
            <a href="http://www.ece.unm.edu/~shu/">http://www.ece.unm.edu/~shu/</a><br>
            <br>
            <b>Research Interests:</b><br>
            - Distributed Operating Systems and Resource Scheduling<br>
            - Multimedia Networking, Multicast overlay networks<br>
            - Ad-hoc Wireless and sensor networks
          </p>
        </li>
        <li class="professor">
          <img src="photos/visitors/image003.jpg">
          <div class="bio">
            <h1 class="name">Yukiko Sasaki</h1>
            <h2 class="title">Professor</h2>
          </div>
          <p class="clear">
            Dept. of Digital Media Science<br>
            Schools of Computer and Information Sciences<br>
            Hosei University, Japan<br>
            <a href="http://ciscgi.k.hosei.ac.jp/~sasaki/">http://ciscgi.k.hosei.ac.jp/~sasaki/</a><br>
            <br>
            <b>Research Interests:</b><br>
            - object-oriented machine translation model<br>
            - structure of the lexicon<br>
            - word sense disambiguation (WSD)<br>
            - text understanding grammar<br>
            - development of educational software<br>
          </p>
        </li>
      </ul>
    </div>
    <div class="clear"></div>
  </div>
</div>
<?php include "footer.php"; ?>