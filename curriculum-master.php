<?php $title = "Academics"; include "header.php"; ?>
<div class="two-column">
  <div class="main grid academics">
    <i class="grid-top-left"></i>
    <i class="grid-top-right"></i>
    <i class="grid-bottom-left"></i>
    <i class="grid-bottom-right"></i>
    <table class="curriculum">
      <thead>
    		<tr><th colspan="3">MS in Computer Science and Technology</th></tr>
      </thead>
      <tbody>
        <tr><th>Course No.</th><th>Course Title</th><th>Credits</th></tr>
        <tr><td>B090701</td>
        <td>Modern Scientific Progress and Maxism</td>
        <td>3.0</td>
        </tr>
    		<tr><td>B140701</td>
    		<td>English for Ph.D Student</td>
    		<td>3.0</td>
    		</tr>
    		<tr><td>G090510</td>
    		<td>Introduction of Chinese Culture</td>
    		<td> 2.0</td>
    		</tr>
    		<tr><td>G090511</td>
    		<td>Chinese</td>
    		<td>2.0</td>
    		</tr>
    		<tr><td>S033701</td>
    		<td>Computer Topic Seminar</td>
    		<td>2.0</td>
    		</tr>
    		<tr><td>B020701</td>
    		<td>Intelligence Control of Mechatronical Engineering</td>
    		<td>2.0</td>
    		</tr>
    		<tr><td>B034701</td>
    		<td>Neural Networks</td>
    		<td>2.0</td>
    		</tr>
    		<tr><td>B071703</td>
    		<td>Functional Analysis with Application</td>
    		<td>3.0</td>
    		</tr>
    		<tr><td>B071704</td>
    		<td>Nonlinear Analysis</td>
    		<td>2.0</td>
    		</tr>
    		<tr><td>B080701</td><td>Introduction on Bio-Science</td>
    		<td>2.0</td>
    		</tr>
    		<tr><td>B080702</td>
    		<td>Academic Writing</td>
    		<td>1.0</td>
    		</tr>
    		<tr><td>B120701</td>
    		<td>System Science and System Engineering</td>
    		<td>2.0</td>
    		</tr>
    		<tr><td>B140703</td>
    		<td>Second Foreign Language (Japanese)</td>
    		<td>2.0</td>
    		</tr>
    		<tr><td>B140704</td>
    		<td> Second Foreign Language (German)</td>
    		<td>2.0</td>
    		</tr>
    		<tr><td>C033713</td>
    		<td>Modern Cryptographic Algorithm </td>
    		<td>3.0</td>
    		</tr>
    		<tr><td>C033714</td>
    		<td>Scientific Data Visualization</td>
    		<td>3.0</td>
    		</tr>
    		<tr><td>C033716</td>
    		<td>Programming Language </td>
    		<td>3.0</td>
    		</tr>
    		<tr><td>C033725</td>
    		<td> Image Processing and Machine Vision</td>
    		<td>3.0</td>
    		</tr>
    		<tr><td>C033726</td>
    		<td>Theory and Methods for Statistical Learning</td>
    		<td>3.0</td>
    		</tr>
    		<tr><td>C033728</td>
    		<td>On the Principle of Provable Security</td>
    		<td>3.0</td>
    		</tr>
    		<tr><td>C033730</td>
    		<td>Advanced Ad Hoc Networks</td>
    		<td>2.0</td>
        </tr>
    	</tbody>
    </table>
  </div>
  <div class="sidebar sprite paperclip2">
    <ul>
      <li class="active"><a href="academics.php">Academics</a></li>
      <li><a href="curriculum-bachelor.php">Undergraduate</a></li>
      <li class="active"><a href="curriculum-master.php">Master's Degree</a></li>
      <li><a href="curriculum-phd.php">Ph.D Degree</a></li>
    </ul>
  </div>
</div>
<?php include "footer.php"; ?>