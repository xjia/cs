<?php $title = "Job Opportunities"; include "header.php"; ?>
<div class="two-column">
  <div class="main grid">
    <i class="grid-top-left"></i>
    <i class="grid-top-right"></i>
    <i class="grid-bottom-left"></i>
    <i class="grid-bottom-right"></i>
    <h1>Tenure-Track Position</h1>
    <p>The Department of Computer Science and Engineering (CSE) at Shanghai Jiao Tong University (SJTU) is seeking to fill several tenure-track positions in computer science at the rank of Assistant Professor and above, starting October 2013 and September 2014.</p>
    <p>Shanghai Jiao Tong University is one of the oldest and most prestigious universities in China, and CSE is premier in computer science research and education. Candidates for these positions are sought for the well-recognized computer science program (ACM class) at Zhiyuan College of SJTU which provides an outstanding undergraduate education to a select group of 30 research-oriented students. Over the last ten years, students from the ACM class have won five gold medals in the ACM International Collegiate Programming Contest.</p>
    <p>Professor John Hopcroft, 1986 Turing Award recipient, is chairing the committee on curriculum development and faculty recruiting. Since December 2011, he has spent two months a year teaching at Zhiyuan College. In May 2012, he was appointed Special Counselor to President Jie Zhang.</p>
    <p>An internationally competitive package for salary and benefits will be offered. Strong candidates in all areas will be considered with special consideration given to systems and networking, architecture, machine learning, theory, and security. In addition to the teaching duties at Zhiyuan College’s ACM class, faculty members are required to teach graduate level courses, to supervise Ph.D. students, and to conduct research in the CSE. The overall teaching load is one course per semester.</p>
    <p>SJTU makes a great effort to provide opportunities for the development of young faculty, including a startup research grant. There are a number of sources for additional research funding. The positions are provided in strong cooperation with Microsoft Research Asia (MSRA) with opportunities for research collaborations. Candidates are encouraged to apply to the Thousand Talents Program for extra funding and benefit support. Our equal opportunity and affirmative action program seeks minorities, women, and non-Chinese scientists.</p>
    <p>The criteria for promotion will be professional reputation as judged by international experts in the candidate’s field and excellence in teaching.</p>
    <p>Applications, including vita and the names of three references, should be sent to Professor John Hopcroft (jeh@cs.cornell.edu) and to Bing Li (binglisjtu@sjtu.edu.cn).</p>
    <p>The application deadline is January 31, 2014 for positions starting in September 2014. Applications for starting earlier will be reviewed immediately.</p>
  </div>
  <div class="sidebar sprite paperclip1">
    <ul>
      <li class="active"><a href="job.php">Job Opportunities</a></li>
      <li><a href="map.php">Visit CS</a></li>
      <li><a href="yellow-page.php">Yellow Pages</a></li>
    </ul>
  </div>
</div>
<?php include "footer.php"; ?>