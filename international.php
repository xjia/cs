<?php $title = "Global CS"; include "header.php"; ?>
<div class="two-column">
  <div class="main grid">
    <i class="grid-top-left"></i>
    <i class="grid-top-right"></i>
    <i class="grid-bottom-left"></i>
    <i class="grid-bottom-right"></i>
    <h1>Partners</h1>
    <p>We are proud of cooperation with many cutting-edge companies and universities around the world. The cooperation provides qualified teachers and students with not only the state of the art technologies but also firsthand experience in industries. Our industries partner include: IBM, Intel, Microsoft and so on. We have many joint research labs with these companies and other famous universities, such as SJTU-Microsoft Joint Lab of Intelligent Computing and Intelligent Systems, SJTU-IBM Grid Computing Joint Lab, Hitachi Joint lab and so on. We also established joint research labs with TU-Berlin and Saarland University from Germany.</p>
    <p>Since 2000, the department has cooperated with the group of Program theory of the university of Tokyo, with NICT on machine learning and computational linguistics, with RIKEN on brain-like computing and machine intelligence for a long history. Many teachers of the department have overseas background. They got their Ph. D in Japan, American, Germany and France. In recent years, many young talented people have jointed the department from abroad.</p>
    <p>Students of our department have many opportunities to join internships abroad. Students have chance to attend Double Master degrees program with TU Berlin or Ersmus Mundus program (European Masters Program in Language and communication technologies). The international program will ensure that our students are at the forefront of understanding practical engineering issues in a global environment.</p>
  </div>
  <div class="sidebar sprite paperclip2">
    <ul>
      <li class="active"><a href="international.php">Global CS</a></li>
      <li><a href="welcome.php">Welcome Message</a></li>
      <li><a href="mission.php">Mission</a></li>
      <li><a href="history.php">History</a></li>
      <li><a href="organization.php">Organization</a></li>
    </ul>
  </div>
</div>
<?php include "footer.php"; ?>