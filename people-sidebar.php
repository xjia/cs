<ul>
  <li<?php echo active($people_type, "Faculty"); ?>><a href="people.php?type=Faculty">Faculty</a></li>
  <li<?php echo active($people_type, "Research Staff"); ?>><a href="people.php?type=Research Staff">Research Staff</a></li>
  <li<?php echo active($people_type, "Adjunct Professor"); ?>><a href="adjunct-professors.php">Adjunct Professors</a></li>
  <li<?php echo active($people_type, "Visiting Scholar"); ?>><a href="visiting-scholars.php">Visiting Scholars</a></li>
  <li<?php echo active($people_type, "General Office"); ?>><a href="people.php?type=General Office">General Office</a></li>
  <li<?php echo active($people_type, "Computer Center"); ?>><a href="people.php?type=Computer Center">Computer Center</a></li>
</ul>