tinymce.init({
  selector: "h1.editable",
  inline: true,
  toolbar: "undo redo",
  menubar: false
});

tinymce.init({
  selector: "span.editable",
  inline: true,
  toolbar: "undo redo",
  menubar: false
});

tinymce.init({
  selector: "div.editable",
  inline: true,
  plugins: [
    "advlist autolink lists link image charmap print preview anchor",
    "searchreplace visualblocks code fullscreen",
    "insertdatetime media table contextmenu paste"
  ],
  toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
});

function save(newsId, type) {
  $.blockUI();
  $.post('save-news.php', {
    'id': newsId,
    'title': $('#newsTitle').text(),
    'date': $('#newsDate').text(),
    'content': $('#newsContent').html()
  })
  .done(function(data) {
    data = JSON.parse(data);
    if (data && data.status === 'ok') {
      window.onbeforeunload = function() {}
      window.location = type + '.php?id=' + newsId;
    }
    else if (data && data.status === 'error' && data.message) {
      alert(data.message);
    }
    else {
      alert('unknown error');
      console.log(data);
    }
  })
  .fail(function() { alert('unknown failure'); })
  .always($.unblockUI);
}

function saveNews(newsId) {
  save(newsId, 'news');
}

function saveEvent(newsId) {
  save(newsId, 'event');
}

function saveAnnouncement(newsId) {
  save(newsId, 'announcement');
}

function create(type) {
  $.blockUI();
  $.post('insert-news.php', {
    'type': type,
    'title': $('#newsTitle').text(),
    'date': $('#newsDate').text(),
    'content': $('#newsContent').html()
  })
  .done(function(data) {
    data = JSON.parse(data);
    if (data && data.status === 'ok' && data.newsId) {
      window.onbeforeunload = function() {}
      window.location = type + '.php?id=' + data.newsId;
    }
    else if (data && data.status === 'error' && data.message) {
      alert(data.message);
    }
    else {
      alert('unknown error');
      console.log(data);
    }
  })
  .fail(function() { alert('unknown failure'); })
  .always($.unblockUI);
}

window.onbeforeunload = function(e) {
  var message = '确定要离开当前页面吗？';
  if (typeof e == 'undefined') {
    e = window.event;
  }
  if (e) {
    e.returnValue = message;
  }
  return message;
}