tinymce.init({
  selector: "span.editable",
  inline: true,
  toolbar: "undo redo",
  menubar: false
});

tinymce.init({
  selector: "p.editable",
  inline: true,
  plugins: [
    "advlist autolink lists link image charmap print preview anchor",
    "searchreplace visualblocks code fullscreen",
    "insertdatetime media table contextmenu paste"
  ],
  toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
});

function save(staffId) {
  $.blockUI();
  $.post('save-staff.php', {
    'id': staffId,
    'FamilyName': $('#FamilyName').text(),
    'FirstName': $('#FirstName').text(),
    'Position': $('#Position').text(),
    'Degree': $('#Degree').text(),
    'OfficeTel': $('#OfficeTel').text(),
    'OfficeSiteBuilding': $('#OfficeSiteBuilding').text(),
    'OfficeSiteRoom': $('#OfficeSiteRoom').text(),
    'Email': $('#Email').text(),
    'OtherEmail': $('#OtherEmail').text(),
    'OtherLink': $('#OtherLink').text(),
    'ResearchArea': $('#ResearchInterest').text(),
    'ResearchInterest': $('#ResearchInterest').html(),
    'EducationBackground': $('#EducationBackground').html(),
    'WorkExperience': $('#WorkExperience').html(),
    'TeachingAssignment': $('#TeachingAssignment').html(),
    'Publications': $('#Publications').html(),
    'Awards': $('#Awards').html(),
    'ProfessionalService': $('#ProfessionalService').html(),
    'Grants': $('#Grants').html(),
    'Duty': $('#Duty').text()
  })
  .done(function(data) {
    data = JSON.parse(data);
    if (data && data.status === 'ok') {
      window.onbeforeunload = function() {}
      window.location = 'people-detail.php?id=' + staffId;
    }
    else if (data && data.status === 'error' && data.message) {
      alert(data.message);
    }
    else {
      alert('unknown error');
      console.log(data);
    }
  })
  .fail(function() { alert('unknown failure'); })
  .always($.unblockUI);
}

window.onbeforeunload = function(e) {
  var message = '确定要离开当前页面吗？';
  if (typeof e == 'undefined') {
    e = window.event;
  }
  if (e) {
    e.returnValue = message;
  }
  return message;
}