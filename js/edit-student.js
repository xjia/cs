tinymce.init({
  selector: "span.editable",
  inline: true,
  toolbar: "undo redo",
  menubar: false
});

tinymce.init({
  selector: "img.editable",
  inline: true,
  plugins: "image media contextmenu",
  toolbar: "undo redo",
  menubar: false
});

function save(studentId) {
  $.blockUI();
  $.post('save-student.php', {
    'id': studentId,
    'FamilyName': $('#FamilyName').text(),
    'FirstName': $('#FirstName').text(),
    'ClassNum': $('#ClassNum').text(),
    'Qmd': $('#Qmd').text(),
    'ContactInfo': $('#ContactInfo').text(),
    'Email': $('#Email').text(),
    'Photo': $('#Photo').data('mce-src')
  })
  .done(function(data) {
    data = JSON.parse(data);
    if (data && data.status === 'ok') {
      window.onbeforeunload = function() {}
      window.location = 'students.php#student' + studentId;
    }
    else if (data && data.status === 'error' && data.message) {
      alert(data.message);
    }
    else {
      alert('unknown error');
      console.log(data);
    }
  })
  .fail(function() { alert('unknown failure'); })
  .always($.unblockUI);
}

window.onbeforeunload = function(e) {
  var message = '确定要离开当前页面吗？';
  if (typeof e == 'undefined') {
    e = window.event;
  }
  if (e) {
    e.returnValue = message;
  }
  return message;
}