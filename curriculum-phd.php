<?php $title = "Academics"; include "header.php"; ?>
<div class="two-column">
  <div class="main grid academics">
    <i class="grid-top-left"></i>
    <i class="grid-top-right"></i>
    <i class="grid-bottom-left"></i>
    <i class="grid-bottom-right"></i>
    <table class="curriculum">
      <thead>
        <tr><th colspan="3">PhD in Computer Science and Technology</th></tr>
      </thead>
  		<tbody>
      	<tr><th>Course No.</th><th>Course Title</th><th>Credits</th></tr>
    		<tr><td>G071552</td><td>Applied Modern Algebra</td>
    		<td>3.0</td>
    		</tr>  
    		<tr><td>G071555</td>
    		<td>Matrix Theory</td>
    		<td>3.0</td>
    		</tr>  		
    		<tr><td>G071557 </td>
    		<td> Graph and Networks</td>
    		<td>2.0</td>
    		</tr>  	
    		<tr><td>G071564</td>
    		<td>Applied Stochastic Processes</td>
    		<td>3.0</td>
    		</tr>
    		<tr><td>G090501</td>
    		<td>Dialectics </td>
    		<td>2.0</td>
    		</tr>
    		<tr><td>G090503</td>
    		<td>Scientific Socialism in Theory and Practice</td>
    		<td>1.0</td>
    		</tr>
    		<tr><td>G090510</td>
    		<td>Introduction of Chinese Culture</td>
    		<td>2.0</td>
    		</tr>
  	    <tr><td>G090511</td>
  	    <td>Chinese</td>
  	    <td>2.0</td>
  	    </tr>
    		<tr><td>G140501</td>
    		<td>English</td>
    		<td>3.0</td>
    		</tr>
    		<tr><td>X033499</td>
    		<td>Special English</td>
    		<td>1.0</td>
    		</tr>
    		<tr><td>X033503</td>
    		<td>Advanced Computer Architecture</td>
    		<td>3.0</td>
    		</tr>
    		<tr><td>X033505</td>
    		<td>Applied Logic</td>
    		<td>3.0</td>
    		</tr>
    		<tr><td>X033506</td>
    		<td>The Theory of Computation,Foundations</td>
    		<td>3.0</td>
    		</tr>
    		<tr><td>X033518</td>
    		<td>Programming Language</td>
    		<td>3.0</td>
    		</tr>
    		<tr><td>X033521</td>
    		<td>Distributed Systems </td>
    		<td>3.0</td>
    		</tr>
    		<tr><td>X033522</td>
    		<td>Advanced Dababase Techniques</td>
    		<td>3.0</td>
    		</tr>
    		<tr><td>X033524</td>
    		<td>Statistical Learning and Inference</td>
    		<td>3.0</td>
    		</tr>
    		<tr><td>X033525</td>
    		<td>Machine Learning</td>
    		<td>3.0</td>
    		</tr>
    		<tr><td>X033528</td>
    		<td>Fundamentally Algebraic</td>
    		<td>3.0</td>
    		</tr>
    		<tr><td>X033530</td>
    		<td>Cryptographic Algorithms and Protocols</td>
    		<td>3.0</td>
    		</tr>
    		<tr><td>X033531</td>
    		<td>Security Engineering</td>
    		<td>2.0</td>
    		</tr>
    		<tr><td>X033532</td>
    		<td>Coding and Information Theory</td>
    		<td>3.0</td>
    		</tr>
    		<tr><td>X033534</td>
    		<td>Concurrency Theory</td>
    		<td>3.0</td>
    		</tr>
    		<tr><td>X033535</td>
    		<td>Analysis and Design of Algorithms</td>
    		<td>3.0</td>
    		</tr>
    		<tr><td>X033536</td>
    		<td>Applied Algebraic</td>
    		<td>3.0</td>
    		</tr>
    		<tr><td>F033563</td>
    		<td>soft computing</td>
    		<td>3.0</td>
    		</tr>
    		<tr><td>F033565</td>
    		<td>Programming Language</td>
    		<td>3.0</td>
    		</tr>
    		<tr><td>F033567</td>
    		<td>Network Computing</td>
    		<td>3.0</td>
    		</tr>
    		<tr><td>F033569</td>
    		<td>natural language understanding</td>
    		<td>3.0</td>
    		</tr>
    		<tr><td>F033570</td>
    		<td> Computational Linguistics</td>
    		<td>3.0</td>
    		</tr>
    		<tr><td>F033572</td>
    		<td>Digital Image Processing</td>
    		<td>3.0</td>
    		</tr>
    		<tr><td>F033573</td>
    		<td>Scientific Computation Visualization</td>
    		<td>3.0</td>
    		</tr>
    		<tr><td>F033574</td>
    		<td>Neural Network Principles and Application</td>
    		<td>3.0</td>
    		</tr>
    		<tr><td>F033575</td>
    		<td>Internet Technology</td>
    		<td>3.0</td>
    		</tr>
    		<tr><td>F033576</td>
    		<td>IT Project Management </td>
    		<td>3.0</td>
    		</tr>
    		<tr><td>F033581</td>
    		<td>Introduction to Network Security </td>
    		<td>3.0</td>
    		</tr>
    		<tr><td>F033583</td>
    		<td>WEB Search and Mining</td>
    		<td>2.0</td>
    		</tr>
    		<tr><td>F033584</td>
    		<td>Computational Number Theory</td>
    		<td>2.0</td>
    		</tr>
    		<tr><td>F033585</td>
    		<td>Wireless Communications and Sensor</td>
    		<td>2.0</td>
    		</tr>
    		<tr><td>F033587</td>
    		<td>Semantic Web</td>
    		<td>2.0</td>
    		</tr>
    		<tr><td>F037517</td>
    		<td> Software System Architecture</td>
    		<td>2.0</td>
    		</tr>
    		<tr><td>F037518</td>
    		<td>Object-oriented Design</td>
    		<td>2.0</td>
    		</tr>
    		<tr><td>F037533</td>
    		<td>Analyzing and Designing Embedded System</td>
    		<td>3.0</td>
    		</tr>
    		<tr><td>F037534</td>
    		<td>Advanced Software Development Technology</td>
    		<td>2.0</td>
    		</tr>
    		<tr><td>S033501</td>
    		<td>Academic Reports</td>
    		<td>2.0</td>
    		</tr>
    		<tr><td>X032503</td>
    		<td>Pattern Recognition</td>
    		<td>3.0</td>
    		</tr>
    		<tr><td>X033514</td>
    		<td>Computer Graphics</td>
    		<td>3.0</td>
    		</tr>
    		<tr><td>X033517</td>
    		<td>Computer Networks</td>
    		<td>3.0</td>
    		</tr>
     		<tr><td>X033523</td>
     		<td> Advanced Technique of Translate and Edit</td>
     		<td>2.0</td>
     		</tr>
    		<tr><td>X033526</td>
    		<td>Bioinformatics</td>
    		<td>3.0</td>
    		</tr>
    		<tr><td>X034518</td>
    		<td> Communication Principle and Systems </td>
    		<td>3.0</td>
    		</tr>
    		<tr><td>X034522</td>
    		<td>Modern Signal Processing</td>
    		<td>3.0</td>
    		</tr>
    		<tr><td>X037502</td>
    		<td>Advanced Software Engineering </td>
    		<td>3.0</td>
    		</tr>
    	</tbody>
    </table>
  </div>
  <div class="sidebar sprite paperclip2">
    <ul>
      <li class="active"><a href="academics.php">Academics</a></li>
      <li><a href="curriculum-bachelor.php">Undergraduate</a></li>
      <li><a href="curriculum-master.php">Master's Degree</a></li>
      <li class="active"><a href="curriculum-phd.php">Ph.D Degree</a></li>
    </ul>
  </div>
</div>
<?php include "footer.php"; ?>