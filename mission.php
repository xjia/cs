<?php $title = "Mission"; include "header.php"; ?>
<div class="two-column">
  <div class="main grid">
    <i class="grid-top-left"></i>
    <i class="grid-top-right"></i>
    <i class="grid-bottom-left"></i>
    <i class="grid-bottom-right"></i>
    <h1>Mission</h1>
    <p>Building on a fifty-year tradition, the mission of SJTU Computer Science and Engineering department is to contribute to cutting-edge research and technological innovation at the highest international levels of excellence. The CSE Department is dedicated to provide a unique educational opportunity for our students to learn and master the fundamental knowledge, to identify and solve emerging problems of the future, and to take leadership roles of professional societies, the community, the nation, and the world.</p>
  </div>
  <div class="sidebar sprite paperclip2">
    <ul>
      <li class="active"><a href="mission.php">Mission</a></li>
      <li><a href="welcome.php">Welcome Message</a></li>
      <li><a href="history.php">History</a></li>
      <li><a href="organization.php">Organization</a></li>
      <li><a href="international.php">Global CS</a></li>
    </ul>
  </div>
</div>
<?php include "footer.php"; ?>