<?php
require_once "common.php";

if (!array_key_exists("id", $_GET)) exit;

$stmt = $dbh->prepare("SELECT * FROM staff WHERE StaffID=:id");
$stmt->bindParam(":id", $_GET["id"]);
$stmt->execute();
$person = $stmt->fetch();

$stmt = $dbh->prepare("SELECT lab.LabID AS LabID, lab.LabName AS LabName, lab.DirectionID AS DirectionID FROM lab_staff,lab WHERE lab_staff.StaffID=:id AND lab_staff.LabID=lab.LabID");
$stmt->bindParam(":id", $_GET["id"]);
$stmt->execute();
$labs = $stmt->fetchAll();

$title = $person["Name"];
$people_type = $person["Type"];
$people_page = "people.php?type=$people_type";

include "header.php";
?>
<div class="fullpage">
  <div class="main grid">
    <i class="grid-top-left"></i>
    <i class="grid-top-right"></i>
    <i class="grid-bottom-left"></i>
    <i class="grid-bottom-right"></i>
    <div class="sidebar"><?php include "people-sidebar.php"; ?></div>
    <div class="content">
      <ul class="letters">
        <?php for ($c = 'A'; $c <= 'Z'; $c = chr(ord($c) + 1)): ?>
        <?php if ($c == $person["Name"][0]): ?>
          <li class="highlight"><a href="<?php echo $people_page; ?>#<?php echo $c; ?>"><?php echo $c; ?></a></li>
        <?php else: ?>
          <li class="active"><a href="<?php echo $people_page; ?>#<?php echo $c; ?>"><?php echo $c; ?></a></li>
        <?php endif; ?>
        <?php endfor; ?>
      </ul>
      <div class="detail">
        <div class="detail-left">
          <img src=".<?php echo $person["Photo"]; ?>">
          <h1><?php echo $person["FamilyName"]; ?> <?php echo $person["FirstName"]; ?></h2>
          <h2><?php echo $person["Position"]; ?> <?php echo $person["Degree"]; ?></h2>
          <ul>
            <li>Office Tel: <?php echo $person["OfficeTel"]; ?></li>
            <li>Office Site: <?php echo $person["OfficeSiteBuilding"]; ?>-<?php echo $person["OfficeSiteRoom"]; ?></li>
            <li>
              Email:
              <?php
              echo str_replace('@', '[at]', $person["Email"]);
              if (strlen($person["OtherEmail"]) > 0)
                echo ', ' . str_replace('@', '[at]', $person["OtherEmail"]);
              ?>
            </li>
            <li>
              Lab:
              <?php foreach ($labs as $lab): ?>
                <a href="labs.php?id=<?php echo $lab['DirectionID']; ?>#lab<?php echo $lab['LabID']; ?>"><?php echo $lab['LabName']; ?></a>
              <?php endforeach; ?>
            </li>
          </ul>
        </div>
        <div class="detail-right">
          <?php if (strlen(trim($person["OtherLink"])) > 0): ?>
            <h2>Homepage</h2>
            <p><?php echo trim($person["OtherLink"]); ?></p>
          <?php endif; ?>

          <?php if (strlen(trim($person["ResearchInterest"])) > 0): ?>
            <h2>Research Interest</h2>
            <p><?php echo nl2br(trim($person["ResearchInterest"])); ?></p>
          <?php endif; ?>

          <?php if (strlen(trim($person["EducationBackground"])) > 0): ?>
            <h2>Education Background</h2>
            <p><?php echo nl2br(trim($person["EducationBackground"])); ?></p>
          <?php endif; ?>

          <?php if (strlen(trim($person["WorkExperience"])) > 0): ?>
            <h2>Work Experience</h2>
            <p><?php echo nl2br(trim($person["WorkExperience"])); ?></p>
          <?php endif; ?>

          <?php if (strlen(trim($person["TeachingAssignment"])) > 0): ?>
            <h2>Teaching Assignment</h2>
            <p><?php echo nl2br(trim($person["TeachingAssignment"])); ?></p>
          <?php endif; ?>

          <?php if (strlen(trim($person["Publications"])) > 0): ?>
            <h2>Publications</h2>
            <p><?php echo nl2br(trim($person["Publications"])); ?></p>
          <?php endif; ?>

          <?php if (strlen(trim($person["Awards"])) > 0): ?>
            <h2>Awards</h2>
            <p><?php echo nl2br(trim($person["Awards"])); ?></p>
          <?php endif; ?>

          <?php if (strlen(trim($person["ProfessionalService"])) > 0): ?>
            <h2>Professional Service</h2>
            <p><?php echo nl2br(trim($person["ProfessionalService"])); ?></p>
          <?php endif; ?>

          <?php if (strlen(trim($person["Grants"])) > 0): ?>
            <h2>Grants</h2>
            <p><?php echo nl2br(trim($person["Grants"])); ?></p>
          <?php endif; ?>
        </div>
        <div class="clear"></div>
      </div>
    </div>
    <div class="clear"></div>
  </div>
</div>
<?php include "footer.php"; ?>