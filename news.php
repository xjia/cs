<?php
require_once "common.php";

if (!array_key_exists("id", $_GET)) exit;

$stmt = $dbh->prepare("SELECT * FROM news WHERE NewsID=:id");
$stmt->bindParam(":id", $_GET["id"]);
$stmt->execute();
$row = $stmt->fetch();

$title = $row['NewsTitle'];
include "header.php";
?>
<div class="two-column">
  <div class="main grid">
    <i class="grid-top-left"></i>
    <i class="grid-top-right"></i>
    <i class="grid-bottom-left"></i>
    <i class="grid-bottom-right"></i>
    <h1><?php echo $row['NewsTitle']; ?></h1>
    <h2>Date: <?php echo $row['NewsDate']; ?></h2>
    <div><?php echo $row['Content']; ?></div>
    <?php if (is_admin()): ?>
      <a href="edit-news.php?id=<?php echo $_GET["id"]; ?>">[edit]</a>
    <?php endif; ?>
  </div>
  <div class="sidebar sprite paperclip1">
    <ul>
      <li class="active"><a href="news-list.php">News</a></li>
      <li><a href="events.php">Events</a></li>
    </ul>
  </div>
</div>
<?php include "footer.php"; ?>