<?php
function json_exception_handler($exception) {
  echo json_encode(array(
    "status" => "error",
    "message" => $exception->getMessage()
  ));
}

set_exception_handler('json_exception_handler');
