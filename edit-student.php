<?php
if (!array_key_exists("id", $_GET)) exit;

require_once "session-start.php";

if (!($_GET["id"] == $_SESSION["EntityID"] and $_SESSION["EntityType"] == "student")) exit;

require_once "common.php";

$stmt = $dbh->prepare("SELECT * FROM student WHERE StudentId=:id");
$stmt->bindParam(":id", $_GET["id"]);
$stmt->execute();
$student = $stmt->fetch();

$title = "Students";
$javascripts = array(
  "//tinymce.cachefly.net/4.0/tinymce.min.js",
  "//code.jquery.com/jquery-2.0.3.min.js",
  "js/jquery.blockUI.js",
  "js/edit-student.js"
);
include "header.php";
?>
<div class="two-column">
  <div class="main grid">
    <i class="grid-top-left"></i>
    <i class="grid-top-right"></i>
    <i class="grid-bottom-left"></i>
    <i class="grid-bottom-right"></i>
    <table class="alumni">
      <tr>
        <td class="photo">
        <?php if (strpos($student['Photo'], 'http') === 0): ?>
          <img id="Photo" class="editable" src="<?php echo $student['Photo']; ?>">
        <?php else: ?>
          <img id="Photo" class="editable" src=".<?php echo $student['Photo']; ?>">
        <?php endif; ?>
        </td>
        <td class="info">
          <h2><span id="FamilyName" class="editable"><?php echo $student['FamilyName']; ?></span>, <span id="FirstName" class="editable"><?php echo $student['FirstName']; ?></span></h2>
          <p>
            <b>Class Number:</b>
            <span id="ClassNum" class="editable"><?php echo $student['ClassNum']; ?></span>
            <br>
            <b>QMD:</b>
            <span id="Qmd" class="editable"><?php echo nl2br(trim($student['Qmd'])); ?></span>
            <br>
            <b>Contact:</b>
            <span id="ContactInfo" class="editable"><?php echo nl2br(trim($student['ContactInfo'])); ?></span>
            <br>
            <b>Email:</b>
            <span id="Email" class="editable"><?php echo $student['Email']; ?></span>
          </p>
        </td>
      </tr>
    </table>
    <button onclick="save('<?php echo $_GET["id"]; ?>')">Submit</button>
  </div>
  <div class="sidebar sprite paperclip1">
    <ul>
      <li class="active"><a href="students.php">Current PhD Students</a></li>
      <li><a href="announcements.php">Announcements</a></li>
      <li><a href="alumni.php">Distinguished Alumni</a></li>
    </ul>
  </div>
</div>
<?php include "footer.php"; ?>