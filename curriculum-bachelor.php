<?php $title = "Academics"; include "header.php"; ?>
<div class="two-column">
  <div class="main grid academics">
    <i class="grid-top-left"></i>
    <i class="grid-top-right"></i>
    <i class="grid-bottom-left"></i>
    <i class="grid-bottom-right"></i>
    <table class="curriculum">
      <thead>
        <tr><th colspan="3">Undergraduate Curriculum<br>Common Core Required By University</th></tr>
      </thead>
      <tbody>
        <tr><th>Course No.</th><th>Course Title</th><th>Credits</th></tr>
        <tr><td>MA001</td><td>Analytic Geometry and Calculus I</td><td>5</td></tr>
        <tr><td>MA012</td><td>Linear Algebra</td><td>2.5</td></tr>
        <tr><td>PH001</td><td>Physics I</td><td>4</td></tr>
        <tr><td>PH002</td><td>Physics II</td><td>4</td></tr>
        <tr><td>PH006</td><td>Experiment in Physics I</td><td>1.5</td></tr>
        <tr><td>PH007</td><td>Experiment in Physics II</td><td>1.5</td></tr>
        <tr><td>MA002</td><td>Analytic Geometry and Calculus II</td><td>5</td></tr>
        <tr><td>MA031</td><td>Probability and Statistics</td><td>2.5</td></tr>
        <tr><td>MA025</td><td>Functions of Complex variable and Integral Transformation</td><td>2</td></tr>
        <tr><td>MA208</td><td>Discrete Mathematics</td><td>3</td></tr>
        <tr><td>BI001</td><td>Introduction to BioTechnology</td><td>2</td></tr>
        <tr><td>CA001</td><td>Chemistry</td><td>2</td></tr>
        <tr><td>EN001</td><td>English I</td><td>4</td></tr>
        <tr><td>EN002</td><td>English II</td><td>4</td></tr>
        <tr><td>EN003</td><td>English III</td><td>4</td></tr>
        <tr><td>EN004</td><td>English IV</td><td>4</td></tr>
      </tbody>
    </table>
  </div>
  <div class="sidebar sprite paperclip2">
    <ul>
      <li class="active"><a href="academics.php">Academics</a></li>
      <li class="active"><a href="curriculum-bachelor.php">Undergraduate</a></li>
      <li><a href="curriculum-master.php">Master's Degree</a></li>
      <li><a href="curriculum-phd.php">Ph.D Degree</a></li>
    </ul>
  </div>
</div>
<?php include "footer.php"; ?>