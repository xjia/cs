<?php
require_once "common.php";

$stmt = $dbh->prepare("SELECT NewsID,NewsTitle,NewsDate FROM news WHERE newstype='News' ORDER BY NewsDate DESC LIMIT 6");
$stmt->execute();
$news = $stmt->fetchAll();

$stmt = $dbh->prepare("SELECT NewsID,NewsTitle,NewsDate FROM news WHERE newstype='Events' ORDER BY NewsDate DESC LIMIT 6");
$stmt->execute();
$events = $stmt->fetchAll();

$title = "Home";
include "header.php";
?>
<div class="home main grid">
  <i class="grid-top-left"></i>
  <i class="grid-top-right"></i>
  <i class="grid-bottom-left"></i>
  <i class="grid-bottom-right"></i>
  <div class="home-left">
    <div class="home-left-inner">
      <i class="sjtu-photo"></i>
      <h1>Welcome to CSE</h1>
    </div>
  </div>
  <div class="home-right">
    <div class="block">
      <h2 class="section sprite"><span>News</span></h2>
      <ul>
        <?php foreach ($news as $n): ?>
        <li>
          <a href="news.php?id=<?php echo $n['NewsID']; ?>" title="<?php echo htmlspecialchars($n['NewsTitle']); ?>">
            <?php echo shorten_title("({$n['NewsDate']}) {$n['NewsTitle']}", 54); ?>
          </a>
        </li>
        <?php endforeach; ?>
      </ul>
      <a class="more" href="news-list.php">MORE &rarr;</a>
    </div>
    <div class="block">
      <h2 class="section sprite"><span>Events</span></h2>
      <ul>
        <?php foreach ($events as $e): ?>
        <li>
          <a href="event.php?id=<?php echo $e['NewsID']; ?>" title="<?php echo htmlspecialchars($e['NewsTitle']); ?>">
            <?php echo shorten_title("({$e['NewsDate']}) {$e['NewsTitle']}", 54); ?>
          </a>
        </li>
        <?php endforeach; ?>
      </ul>
      <a class="more" href="events.php">MORE &rarr;</a>
    </div>
  </div>
</div>
<div class="home-links">
  <div class="block">
    <h2><em>Info</em> for Visitors</h2>
    <a href="map.php">
      <i id="info-map" class="info-map"></i>
      <a class="to-info-map" href="" onclick="document.getElementById('info-map').style.zIndex=502;document.getElementById('info-yellow-page').style.zIndex=501;return false;"></a>
    </a>
    <a href="yellow-page.php">
      <i id="info-yellow-page" class="info-yellow-page"></i>
      <a class="to-info-yellow-page" href="" onclick="document.getElementById('info-map').style.zIndex=501;document.getElementById('info-yellow-page').style.zIndex=502;return false;"></a>
    </a>
  </div>
  <div class="block m guide">
    <h2><em>CS</em> Guide</h2>
    <ul>
      <li><a href="http://www.cs.sjtu.edu.cn/~dingyue/email.html">Email and Mailing List</a></li>
      <li><a href="http://www.cs.sjtu.edu.cn/~dingyue/php.html">Personal Home Page and ...</a></li>
      <li><a href="http://www.cs.sjtu.edu.cn/~dingyue/printing.html">Printing</a></li>
    </ul>
    <a class="more" href="http://www.cs.sjtu.edu.cn/~dingyue/">MORE &rarr;</a>
  </div>
  <a class="block m" href="job.php">
    <h2><em>Job</em> Opportunities</h2>
    <i class="job"></i>
  </a>
</div>
<?php include "footer.php"; ?>