<?php
require_once "common.php";

if (array_key_exists("type", $_GET)) {
  $people_type = $_GET["type"];
}
else {
  $people_type = "Faculty";
}

$stmt = $dbh->prepare("SELECT * FROM staff WHERE Type=:type ORDER BY Name");
$stmt->bindParam(":type", $people_type);
$stmt->execute();

$people_by_letters = array();
for ($c = 'A'; $c <= 'Z'; $c = chr(ord($c) + 1)) {
  $people_by_letters[$c] = array();
}
while ($row = $stmt->fetch()) {
  $letter = $row['Name'][0];
  $people_by_letters[$letter][] = $row;
}

$title = "People";
include "header.php";
?>
<div class="fullpage">
  <div class="main grid">
    <i class="grid-top-left"></i>
    <i class="grid-top-right"></i>
    <i class="grid-bottom-left"></i>
    <i class="grid-bottom-right"></i>
    <div class="sidebar"><?php include "people-sidebar.php"; ?></div>
    <div class="content">
      <ul class="letters">
        <?php for ($c = 'A'; $c <= 'Z'; $c = chr(ord($c) + 1)): ?>
        <?php if (count($people_by_letters[$c])): ?>
          <li class="active"><a href="#<?php echo $c; ?>"><?php echo $c; ?></a></li>
        <?php else: ?>
          <li><?php echo $c; ?></li>
        <?php endif; ?>
        <?php endfor; ?>
      </ul>
      <div class="list">
        <?php foreach ($people_by_letters as $letter => $people): ?>
        <?php if (count($people) > 0): ?>
          <a class="top" name="<?php echo $letter; ?>" href="#top">TOP</a>
          <h2><?php echo $letter; ?></h2>
          <ul>
          <?php foreach ($people as $person): ?>
            <li class="person">
              <?php if ($person['Type'] == 'General Office' or $person['Type'] == 'Computer Center'): ?>
              <div class="photo"><img src=".<?php echo $person['Photo']; ?>"></div>
              <h3><?php echo $person['Name']; ?></h3>
              <p class="main-duties">
                Main Duties:<br>
                <?php echo nl2br($person['Duty']); ?>
              </p>
              <?php else: ?>
              <div class="photo">
                <a href="people-detail.php?id=<?php echo $person['StaffID']; ?>">
                  <img src=".<?php echo $person['ThumbPhoto']; ?>">
                </a>
              </div>
              <h3>
                <a href="people-detail.php?id=<?php echo $person['StaffID']; ?>">
                  <?php echo $person['Name']; ?>
                  <?php if (strlen($person['Position']) > 0): ?>
                     (<?php echo $person['Position']; ?>)
                  <?php endif; ?>
                </a>
              </h3>
              <a href="people-detail.php?id=<?php echo $person['StaffID']; ?>" class="research-area">
                Research Area:<br>
                <?php echo nl2br($person['ResearchArea']); ?>
              </a>
              <?php endif; ?>
              <ul class="contact-info">
                <li>Office: <?php echo $person['OfficeSiteBuilding']; ?> - <?php echo $person['OfficeSiteRoom']; ?></li>
                <li>Phone: <?php echo $person['OfficeTel']; ?></li>
                <li>Email: <?php echo str_replace('@', '[at]', $person['Email']); ?></li>
              </ul>
              <div class="clear"></div>
            </li>
          <?php endforeach; ?>
          </ul>
        <?php endif; ?>
        <?php endforeach; ?>
      </div>
    </div>
    <div class="clear"></div>
  </div>
</div>
<?php include "footer.php"; ?>